DATE=$(date +%y%m%d)
COMMIT_HASH=$(git rev-parse HEAD | colrm 9)
mvn versions:set -DnewVersion=$DATE-$COMMIT_HASH
mvn clean
mvn package
